#ifndef THOR_PREFILTER_CONTROLLER____
#define THOR_PREFILTER_CONTROLLER____


#include <actionlib/server/action_server.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <thor_prefilter/thor_prefilter.h>
#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <diagnostic_msgs/DiagnosticArray.h>
#include <thread>
#include <mutex>
#include <std_msgs/Int64.h>
#include <std_msgs/Float64.h>
# include <itia_basic_hardware_interface/posveleff_command_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <controller_interface/controller.h>
#include <sensor_msgs/JointState.h>
#include <name_sorting/sort_trajectories.h>

namespace thor
{

  class PrefilterController
  {
  public:
    PrefilterController();
    ~PrefilterController();
    
    virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
    virtual bool update(const ros::Time& time, const ros::Duration& period);
    virtual void starting(const ros::Time& time, const std::vector<double>& qini, const std::vector<double>& Dqini);
    virtual void stopping(const ros::Time& time);
    std::vector<std::string> getJointNames(){return m_joint_names;};
    trajectory_msgs::JointTrajectoryPoint& getTrajectoryPoint(){return m_pnt;};
    
    ros::Duration getTime(){return m_time;};
    ros::Duration getScaledTime(){return m_scaled_time;};
    
  protected:
    void ac_thread_function();
    void actionGoalCallback(  actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction>::GoalHandle gh);
    void actionCancelCallback(actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction>::GoalHandle gh);
    void overrideCallback(const std_msgs::Int64ConstPtr& msg, const std::string& override_name);
    bool m_stop;
    std::map<std::string,double> m_overrides;
    double m_global_override;
    boost::shared_ptr<thor::ThorPrefilter> m_thor_prefilter;
    ros::CallbackQueue m_queue;
    
    int m_is_finished;
    ros::NodeHandle m_nh;
    std::mutex m_mtx;
    
    boost::shared_ptr<actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction>> m_as;
    boost::shared_ptr<actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction>::GoalHandle> m_gh;
    std::thread m_as_thread;
    bool m_preempted;
    ros::NodeHandle m_root_nh;
    ros::NodeHandle m_controller_nh;
    ros::Publisher m_diagnostics_pub;
    ros::Publisher m_target_pub;
    ros::Publisher m_unscaled_target_pub;

    trajectory_msgs::JointTrajectoryPoint m_pnt;
    std::vector<std::string> m_joint_names;
    
    std::size_t m_nax;
    ros::Duration m_scaled_time;
    ros::Duration m_time;
    std::vector<ros::Subscriber> m_override_topic;
    ros::Publisher m_scaled_time_pub;
    ros::Publisher m_ratio_pub;
    bool m_is_in_tolerance;
    bool m_is_in_path_tolerance;
    double m_goal_tolerance;
    double m_path_tolerance;
  };
  

  class PrefilterPosVelEffController : public controller_interface::Controller<hardware_interface::PosVelEffJointInterface>
  {
  public:
    virtual bool init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
    void update(const ros::Time& time, const ros::Duration& period);
    void starting(const ros::Time& time);
    void stopping(const ros::Time& time);
    
  protected:
    hardware_interface::PosVelEffJointInterface* m_hw;
    std::shared_ptr<PrefilterController> m_ctrl;
    ~PrefilterPosVelEffController();
    
    ros::NodeHandle m_root_nh;
    ros::NodeHandle m_controller_nh;
    std::vector<std::string> m_joint_names;
    std::vector<hardware_interface::PosVelEffJointHandle> m_handles;
    unsigned int m_nAx;
  };
  
  class PrefilterPosController : public controller_interface::Controller<hardware_interface::PositionJointInterface>
  {
  public:
    bool init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
    void update(const ros::Time& time, const ros::Duration& period);
    void starting(const ros::Time& time);
    void stopping(const ros::Time& time);
    
  protected:
    hardware_interface::PositionJointInterface* m_hw;
    std::shared_ptr<PrefilterController> m_ctrl;
    ~PrefilterPosController();
    
    ros::NodeHandle m_root_nh;
    ros::NodeHandle m_controller_nh;
    std::vector<std::string> m_joint_names;
    std::vector<hardware_interface::JointHandle> m_handles;

    unsigned int m_nAx;
  };
  
};



#endif
