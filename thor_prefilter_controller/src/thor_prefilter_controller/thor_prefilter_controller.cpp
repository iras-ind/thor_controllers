#include <thor_prefilter_controller/thor_prefilter_controller.h>
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(thor::PrefilterPosController, controller_interface::ControllerBase);
PLUGINLIB_EXPORT_CLASS(thor::PrefilterPosVelEffController, controller_interface::ControllerBase);
namespace thor 
{


/* ===============================================================================
   * ===============================================================================
   * ===============================================================================
   * =============================== PrefilterController ===========================
   * ===============================================================================
   * ===============================================================================
   * =============================================================================== */

PrefilterController::~PrefilterController()
{
  ROS_INFO("Destroying Thor Prefilter Controller");
  m_preempted=true;
  m_stop=true;
  if (m_as_thread.joinable())
  {
    m_as_thread.join();
  }
  m_as.reset();
  ROS_INFO("Destroyed Thor Prefilter Controller");
}

PrefilterController::PrefilterController()
{
  m_stop=false;
  m_global_override=1;

  m_is_in_tolerance=true;
  m_goal_tolerance=0.00001;
  m_path_tolerance=0.01;
  m_preempted=false;
}

bool PrefilterController::init(ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  m_root_nh         = root_nh;
  m_controller_nh   = controller_nh;
  ROS_INFO("[ %s ] Initialization of Thor Prefilter planner",m_controller_nh.getNamespace().c_str());
  
  if (!m_controller_nh.getParam("controlled_joint",m_joint_names))
  {
    ROS_FATAL("controlled_joint not defined");
    return false;
  }
  m_nax=m_joint_names.size();

  std::vector<std::string> overrides;
  if (!m_controller_nh.getParam("overrides",overrides))
  {
    ROS_DEBUG("overrides are not speficied for controllers %s. Using default",m_controller_nh.getNamespace().c_str());
    overrides.push_back("/speed_ovr");
    overrides.push_back("/safe_ovr_1");
    overrides.push_back("/safe_ovr_2");
  }

  //------------------------------------ NP -----------
  m_controller_nh.setCallbackQueue(&m_queue);
  m_diagnostics_pub         = m_controller_nh.advertise<diagnostic_msgs::DiagnosticArray>("/diagnostics",1);
  m_target_pub              = m_controller_nh.advertise<sensor_msgs::JointState>         ("target/joint_states",1);
  m_unscaled_target_pub     = m_controller_nh.advertise<sensor_msgs::JointState>         ("unscaled_joint_target/joint_states",1);
  for (const std::string& override_name: overrides)
  {
    auto cb=boost::bind(&thor::PrefilterController::overrideCallback,this,_1,override_name);
    ros::Subscriber ovr_sub=m_controller_nh.subscribe<std_msgs::Int64>(override_name,1,cb);
    m_override_topic.push_back(ovr_sub);
    m_overrides.insert(std::pair<std::string,double>(override_name,1));
  }
  m_scaled_time_pub = m_controller_nh.advertise<std_msgs::Float64>("/scaled_time",1);
  m_ratio_pub = m_controller_nh.advertise<std_msgs::Float64>("execution_ratio",1);


  //---------------------------------------------------


  ROS_INFO("[ %s ] Successfully Initialization of Thor Prefilter planner",m_controller_nh.getNamespace().c_str());
  return true;
}

void PrefilterController::starting(const ros::Time& time, const std::vector< double >& qini, const std::vector< double >& Dqini)
{
  ROS_INFO("[ %s ] Starting of Thor Prefilter planner",m_controller_nh.getNamespace().c_str());
  assert(m_nax == qini.size());
  assert(m_nax == Dqini.size());
  
  m_pnt.positions   = qini;
  m_pnt.velocities  = Dqini;
  m_pnt.accelerations.resize(m_nax,0);
  m_pnt.effort.resize(m_nax,0);
  m_pnt.time_from_start=ros::Duration(0);
  trajectory_msgs::JointTrajectoryPtr trj(new trajectory_msgs::JointTrajectory());
  trj->points.push_back(m_pnt);
  m_thor_prefilter.reset(new thor::ThorPrefilter());
  m_thor_prefilter->setTrajectory(trj);
  m_scaled_time=ros::Duration(0);;
  m_time=ros::Duration(0);
  int spline_order=1;
  if (!m_controller_nh.getParam("spline_order",spline_order))
  {
    ROS_DEBUG("spline_order is not set, set equal to 1");
    spline_order=1;
  }
  if (spline_order<0)
    spline_order=0;
  m_thor_prefilter->setSplineOrder(spline_order);

  m_as.reset(new actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction>(m_controller_nh, "follow_joint_trajectory",
                                                                                    boost::bind(&thor::PrefilterController::actionGoalCallback,    this,  _1),
                                                                                    boost::bind(&thor::PrefilterController::actionCancelCallback,  this,  _1),
                                                                                    false));
  m_as->start();
  ROS_INFO("[ %s ] Successfully Start of Thor Prefilter planner!",m_controller_nh.getNamespace().c_str());
}

bool PrefilterController::update(const ros::Time& time, const ros::Duration& period)
{
  try
  {
    m_queue.callAvailable();
  }
  catch (std::exception& e)
  {
    ROS_ERROR("something wrong in callbacks: %s", e.what());
    return false;
  }
  
  try
  {
    if( !m_thor_prefilter->interpolate(m_scaled_time,m_pnt,m_global_override) )
    {
      ROS_ERROR_THROTTLE(0.5,"something wrong in interpolation.");
      return false;
    }
    m_scaled_time += period * m_global_override;
    m_time        += period;
    std_msgs::Float64Ptr scaled_msg(new std_msgs::Float64());
    scaled_msg->data=m_scaled_time.toSec();
    m_scaled_time_pub.publish(scaled_msg);

    std_msgs::Float64Ptr ratio_msg(new std_msgs::Float64());
    if (m_thor_prefilter->trjTime().toSec()>0)
      ratio_msg->data=std::min(1.0,m_scaled_time.toSec()/m_thor_prefilter->trjTime().toSec());
    else
      ratio_msg->data=1;
    m_ratio_pub.publish(ratio_msg);


    
    sensor_msgs::JointStatePtr js_msg(new sensor_msgs::JointState());
    js_msg->name = m_joint_names;
    js_msg->position.resize(m_nax);
    js_msg->velocity.resize(m_nax);
    js_msg->effort.resize(m_nax,0);
    js_msg->position      = getTrajectoryPoint().positions;
    js_msg->velocity      = getTrajectoryPoint().velocities;
    js_msg->header.stamp  = ros::Time::now();
    m_target_pub.publish(js_msg);
    
    sensor_msgs::JointStatePtr unscaled_js_msg(new sensor_msgs::JointState());
    trajectory_msgs::JointTrajectoryPoint unscaled_pnt;
    if( !m_thor_prefilter->interpolate(m_scaled_time,unscaled_pnt,1) )
    {
      ROS_ERROR_THROTTLE(0.5,"something wrong in interpolation.");
      return false;
    }

    unscaled_js_msg->name = m_joint_names;
    unscaled_js_msg->position.resize(m_nax);
    unscaled_js_msg->velocity.resize(m_nax);
    unscaled_js_msg->effort.resize(m_nax,0);
    unscaled_js_msg->position      = unscaled_pnt.positions;
    unscaled_js_msg->velocity      = unscaled_pnt.velocities;
    unscaled_js_msg->header.stamp  = ros::Time::now();
    m_unscaled_target_pub.publish(unscaled_js_msg);

    return true;
  }
  catch (std::exception& e)
  {
    ROS_ERROR_STREAM("something wrong: " << e.what()
                     << "(function input: Time: " << time.toSec() << " Duration: " << period.toSec() << ")" );
    return false;
  }
}

void PrefilterController::stopping(const ros::Time& time)
{
  ROS_INFO("Stopping thor");
  if (m_gh)
  {
    m_gh->setCanceled();
  }
  if (m_as_thread.joinable())
  {
    m_as_thread.join();
  }
  m_gh.reset();
  ROS_INFO("Stopped thor successfully!");
}

void PrefilterController::overrideCallback(const std_msgs::Int64ConstPtr& msg, const std::string& override_name)
{
  double ovr;
  if (msg->data>100)
    ovr=1;
  else if (msg->data<0)
    ovr=0;
  else
    ovr=msg->data*0.01;
  m_overrides.at(override_name)=ovr;
  double global_override=1;
  for (const std::pair<std::string,double>& p: m_overrides)
    global_override*=p.second;
  m_global_override=global_override;
}

void PrefilterController::ac_thread_function()
{
  ROS_DEBUG("START ACTION GOAL LOOPING");
  ros::WallRate lp(100);
  while (m_nh.ok())
  {
    if (m_stop)
      break;
    
    lp.sleep();
    if (!m_gh)
    {
      ROS_ERROR("Goal handle is not initialized");
      break;
    }
    
    if ( (m_preempted) ||  (m_gh->getGoalStatus().status == actionlib_msgs::GoalStatus::RECALLED) ||  (m_gh->getGoalStatus().status == actionlib_msgs::GoalStatus::PREEMPTED) )
    {
      ROS_DEBUG("(m_gh->getGoalStatus().status == actionlib_msgs::GoalStatus::PREEMPTED)  = %d",(int)(m_gh->getGoalStatus().status == actionlib_msgs::GoalStatus::PREEMPTED) );
      ROS_DEBUG("m_preempted  = %d",(int)m_preempted );
      ROS_DEBUG("(m_gh->getGoalStatus().status == actionlib_msgs::GoalStatus::RECALLED)  = %d",(int)(m_gh->getGoalStatus().status == actionlib_msgs::GoalStatus::RECALLED) );
      
      control_msgs::FollowJointTrajectoryResult result;
      diagnostic_msgs::DiagnosticArray diag_msg;
      diag_msg.header.stamp=ros::Time::now();
      diag_msg.status.resize(1);
      diag_msg.status.at(0).name="INTERPOLATOR";
      diag_msg.status.at(0).hardware_id=m_root_nh.getNamespace();
      diag_msg.status.at(0).level=diagnostic_msgs::DiagnosticStatus::OK;
      if (m_preempted)
        diag_msg.status.at(0).message="preempted";
      else
        diag_msg.status.at(0).message="cancelled";
      diag_msg.status.at(0).values.resize(0);
      m_diagnostics_pub.publish(diag_msg);
      result.error_code=0;
      result.error_string="preempted";
      m_gh->setRejected(result);
      m_preempted=false;
      ROS_DEBUG("preempted old goal DONE");
      
      break;
    }
    
    if ((m_is_finished==1) || (((m_scaled_time-m_thor_prefilter->trjTime()).toSec()>0) && m_is_in_tolerance))
    {
      control_msgs::FollowJointTrajectoryResult result;
      
      diagnostic_msgs::DiagnosticArray diag_msg;
      diag_msg.header.stamp=ros::Time::now();
      diag_msg.status.resize(1);
      diag_msg.status.at(0).name="INTERPOLATOR";
      diag_msg.status.at(0).hardware_id=m_root_nh.getNamespace();
      diag_msg.status.at(0).level=diagnostic_msgs::DiagnosticStatus::OK;
      diag_msg.status.at(0).message="all is ok";
      diag_msg.status.at(0).values.resize(0);
      m_diagnostics_pub.publish(diag_msg);
      
      m_gh->setSucceeded(result);
      ROS_DEBUG("EXIT CLEAN");

      break;
    }
    else if (m_is_finished==-2)
    {
      control_msgs::FollowJointTrajectoryResult result;
      result.error_code=-4;
      result.error_string="Some problem occurs";
      
      diagnostic_msgs::DiagnosticArray diag_msg;
      diag_msg.header.stamp=ros::Time::now();
      diag_msg.status.resize(1);
      diag_msg.status.at(0).name="INTERPOLATOR";
      diag_msg.status.at(0).hardware_id=m_root_nh.getNamespace();
      diag_msg.status.at(0).level=diagnostic_msgs::DiagnosticStatus::ERROR;
      diag_msg.status.at(0).message="Some problem occurs";
      diag_msg.status.at(0).values.resize(1);
      diag_msg.status.at(0).values.at(0).key="INTERPOLATOR_TYPE";
      diag_msg.status.at(0).values.at(0).value="FIR planner";
      m_diagnostics_pub.publish(diag_msg);
      
      m_gh->setAborted(result);
      break;
    }
    
  }
  m_gh.reset();
}

void PrefilterController::actionGoalCallback(actionlib::ActionServer< control_msgs::FollowJointTrajectoryAction >::GoalHandle gh)
{
  ROS_DEBUG("received a goal");
  auto goal=gh.getGoal();

  boost::shared_ptr<actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction>::GoalHandle> current_gh;
  
  if (m_gh)
  {
    // PREEMPTED OLD GOAL
    
    ROS_DEBUG("preempting old goal");
    m_gh->setAborted();
    m_preempted=true;
    
    if (m_as_thread.joinable())
      m_as_thread.join();
    ROS_DEBUG("STOPPED");
    m_preempted=false;
  }
  else
  {
    ROS_DEBUG("No goals running yet");
  }
  
  current_gh.reset(new actionlib::ActionServer<control_msgs::FollowJointTrajectoryAction>::GoalHandle(gh));
  m_gh=current_gh;
  unsigned  int nPnt = goal->trajectory.points.size();
  
  if (nPnt==0)
  {
    ROS_DEBUG("TRAJECTORY WITH NO POINT");
    control_msgs::FollowJointTrajectoryResult result;
    m_gh->setAccepted();
    current_gh->setSucceeded(result);
    m_gh.reset();
    return;
  }
  
  trajectory_msgs::JointTrajectoryPtr trj(new trajectory_msgs::JointTrajectory());
  
  if (!trajectory_processing::sort_trajectory(m_joint_names,goal->trajectory,*trj))
  {
    ROS_ERROR("name are different");
    m_gh->setAborted();
    if (m_as_thread.joinable())
      m_as_thread.join();
    return;
  }

  m_mtx.lock();
  m_thor_prefilter->setTrajectory(trj);
  m_scaled_time=ros::Duration(0);
  m_time=ros::Duration(0);
  
  ROS_DEBUG("Starting managing new goal");
  m_is_finished=0;
  m_mtx.unlock();
  m_gh->setAccepted();
  
  if (m_as_thread.joinable())
    m_as_thread.join();
  
  m_as_thread = std::thread(&PrefilterController::ac_thread_function,this);
  
}

void PrefilterController::actionCancelCallback(actionlib::ActionServer< control_msgs::FollowJointTrajectoryAction >::GoalHandle gh)
{
  ROS_DEBUG("Stopping active goal");
  if (m_gh)
  {
    m_gh->setCanceled();
    if (m_as_thread.joinable())
      m_as_thread.join();
    m_gh.reset();
    
    m_mtx.lock();
    
    trajectory_msgs::JointTrajectoryPtr trj(new trajectory_msgs::JointTrajectory());
    trj->points.push_back(m_pnt);
    m_thor_prefilter->setTrajectory(trj);
    m_scaled_time=ros::Duration(0);
    m_time=ros::Duration(0);
    m_mtx.unlock();
    
  }
  else
    ROS_WARN("no goal to cancel");
}


/* ===============================================================================
 * ===============================================================================
 * ===============================================================================
 * ========================== PrefilterPosVelEffController =======================
 * ===============================================================================
 * ===============================================================================
 * =============================================================================== */

bool PrefilterPosVelEffController::init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  m_ctrl.reset(new thor::PrefilterController());
  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw = hw;
  if (!m_ctrl->init(root_nh,controller_nh))
    return false;
  
  m_joint_names=m_ctrl->getJointNames();
  m_nAx=m_joint_names.size();
  for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
    m_handles.push_back(m_hw->getHandle(m_joint_names.at(iDim)));

  return true;
}

void PrefilterPosVelEffController::starting(const ros::Time& time)
{
  std::vector<double> qini(m_nAx);
  std::vector<double> Dqini(m_nAx,0);
  for (unsigned int idx=0;idx<m_nAx;idx++)
  {
    qini.at(idx)=m_handles.at(idx).getPosition();
    Dqini.at(idx)=m_handles.at(idx).getVelocity();
  }
  m_ctrl->starting(time,qini,Dqini);
}

void PrefilterPosVelEffController::stopping(const ros::Time& time)
{
  m_ctrl->stopping(time);
}

void PrefilterPosVelEffController::update(const ros::Time& time, const ros::Duration& period)
{
  if (m_ctrl->update(time,period))
  {
    try
    {
      for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
      {
        m_handles.at(iDim).setCommandPosition( m_ctrl->getTrajectoryPoint().positions.at(iDim));
        m_handles.at(iDim).setCommandVelocity( m_ctrl->getTrajectoryPoint().velocities.at(iDim));
        if (m_ctrl->getTrajectoryPoint().effort.size()==m_nAx)
          m_handles.at(iDim).setCommandEffort(   m_ctrl->getTrajectoryPoint().effort.at(iDim));
      }
    }
    catch (std::exception& ex)
    {
      ROS_ERROR("Error in PrefilterPosVelEffController::update: %s",ex.what());
    }
  }
  else
    ROS_ERROR_THROTTLE(1, "[%s] PLANNER NOT CONFIGURED", m_controller_nh.getNamespace().c_str());
}


PrefilterPosVelEffController::~PrefilterPosVelEffController()
{

}


/* ===============================================================================
 * ===============================================================================
 * ===============================================================================
 * ============================= PrefilterPosController ==========================
 * ===============================================================================
 * ===============================================================================
 * =============================================================================== */

bool PrefilterPosController::init(hardware_interface::PositionJointInterface* hw,
                                  ros::NodeHandle& root_nh,
                                  ros::NodeHandle& controller_nh)
{
  m_ctrl.reset(new thor::PrefilterController());
  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw = hw;
  if (!m_ctrl->init(root_nh,controller_nh))
    return false;
  
  m_joint_names=m_ctrl->getJointNames();
  m_nAx=m_joint_names.size();


  for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
    m_handles.push_back(m_hw->getHandle(m_joint_names.at(iDim)));
  return true;
}

void PrefilterPosController::starting(const ros::Time& time)
{
  ROS_INFO("%s: Controller starting. %u Joints, %zu handles ",m_controller_nh.getNamespace().c_str(),m_nAx, m_handles.size());

  std::vector<double> qini(m_nAx);
  std::vector<double> Dqini(m_nAx,0);
  for (unsigned int idx=0;idx<m_nAx;idx++)
  {
    qini.at(idx)=m_handles.at(idx).getPosition();
    Dqini.at(idx)=m_handles.at(idx).getVelocity();
  }
  m_ctrl->starting(time,qini,Dqini);
}

void PrefilterPosController::stopping(const ros::Time& time)
{
  m_ctrl->stopping(time);
}

void PrefilterPosController::update(const ros::Time& time, const ros::Duration& period)
{
  
  
  if (m_ctrl->update(time,period))
  {
    try
    {
      for (unsigned int iDim = 0;iDim<m_nAx;iDim++)
        m_handles.at(iDim).setCommand( m_ctrl->getTrajectoryPoint().positions.at(iDim));
      
    }
    catch (std::exception& ex)
    {
      ROS_ERROR("Error in PrefilterPosController::update: %s",ex.what());
      return;
    }
  }
  else
    ROS_ERROR_THROTTLE(1, "[%s] PLANNER NOT CONFIGURED", m_controller_nh.getNamespace().c_str());
  
  
  
}


PrefilterPosController::~PrefilterPosController()
{
  
}







}
