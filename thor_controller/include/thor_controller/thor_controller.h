#ifndef THOR_CONTROLLER_____
#define THOR_CONTROLLER_____

#include <thor_math/thor_math.h>
#include <thor_prefilter_controller/thor_prefilter_controller.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64.h>

namespace thor 
{
 
  class ThorController: public PrefilterController
  {
  public:
    ThorController();
    ~ThorController();
    
    virtual bool init(ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
    virtual bool update(const ros::Time& time, const ros::Duration& period);
    virtual void starting(const ros::Time& time, const std::vector<double>& qini, const std::vector<double>& Dqini);
    virtual void stopping(const ros::Time& time);
  protected:
    
    PrefilterController m_prefilter;
    math::ThorQP m_thorQP;
    rosdyn::ChainPtr m_chain;
    Eigen::VectorXd m_prediction_time;
    
    Eigen::VectorXd m_initial_state;
    Eigen::VectorXd m_target_Dq;
    double m_target_scaling;
    Eigen::VectorXd m_next_Q;
    Eigen::VectorXd m_next_acc;
    double m_scaling;
    
    double m_out_of_path_scaling;
    trajectory_msgs::JointTrajectoryPoint m_prefilter_pnt;
    
    ros::Publisher m_tolerance_pub;
    ros::Publisher m_scaling_pub;
    
  };
  
  class ThorPosVelEffController : public PrefilterPosVelEffController
  {
  public:
    virtual bool init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  };

  class ThorPosController : public PrefilterPosController
  {
  public:
    virtual bool init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh);
  };  
}

#endif
