#include <thor_controller/thor_controller.h>
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(thor::ThorPosController, controller_interface::ControllerBase);
PLUGINLIB_EXPORT_CLASS(thor::ThorPosVelEffController, controller_interface::ControllerBase);

namespace thor 
{
  
ThorController::ThorController():
thor::PrefilterController::PrefilterController()
{
  
}

ThorController::~ThorController()
{

}

bool ThorController::init(ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  thor::PrefilterController::init(root_nh,controller_nh);
  m_out_of_path_scaling=1;
  unsigned int nc=5;
  double control_horizon=1;
  double st=0.008;
  
  if (!m_controller_nh.getParam("control_horizon",control_horizon))
  {
    ROS_DEBUG("control_horizon is not set, set equal to 1");
  }
  if (!m_controller_nh.getParam("period",st))
  {
    ROS_DEBUG("period is not set, set equal to 0.008");
    st=0.008;
  }
  int tmp;
  if (!m_controller_nh.getParam("prediction_size",tmp))
  {
    ROS_DEBUG("prediction_size is not set, set equal to 5");
    tmp=5;
  }
  nc=tmp;
  
  m_tolerance_pub = m_controller_nh.advertise<std_msgs::Bool>("in_tolerance",1);
  m_scaling_pub   = m_controller_nh.advertise<std_msgs::Float64>("scaling",1);
  double lambda_acc=1.0e-40;
  double lambda_tau=0;
  double lambda_scaling=1.0e-1;//1e-2;
  double lambda_clik=1.0e2;//1e0;
  double lambda_jerk=1.0e-14;//1e0;

  if (!m_controller_nh.getParam("lambda_acc",lambda_acc))
  {
    ROS_ERROR("lambda_acc is not set. default: %f", lambda_acc);
  }
  if (!m_controller_nh.getParam("lambda_tau",lambda_tau))
  {
    ROS_ERROR("lambda_tau is not set. default: %f", lambda_tau);
  }
  if (!m_controller_nh.getParam("lambda_scaling",lambda_scaling))
  {
    ROS_ERROR("lambda_scaling is not set. default: %f", lambda_scaling);
  }
  if (!m_controller_nh.getParam("lambda_clik",lambda_clik))
  {
    ROS_ERROR("lambda_clik is not set. default: %f", lambda_clik);
  }
  if (!m_controller_nh.getParam("lambda_jerk",lambda_jerk))
  {
    ROS_ERROR("lambda_jerk is not set. default: %f", lambda_jerk);
  }
  bool enable_pos_bound;
  if (!m_controller_nh.getParam("activate_pos_limits",enable_pos_bound))
  {
    enable_pos_bound=false;
    ROS_ERROR("Position limits not activated by default");
  }
  m_thorQP.activatePositionBounds(enable_pos_bound);
  bool enable_torque_bound;
  if (!m_controller_nh.getParam("activate_torque_limits",enable_torque_bound))
  {
    enable_pos_bound=false;
    ROS_ERROR("Torque limits not activated by default");
  }
  m_thorQP.activateTorqueBounds(enable_torque_bound);

  if (enable_torque_bound==true)
  {
    std::string base_frame;
    if (!m_controller_nh.getParam("base_frame", base_frame))
    {
      ROS_ERROR("base_frame not defined");
      return false;
    }
    std::string tool_frame;
    if (!m_controller_nh.getParam("tool_frame", tool_frame))
    {
      ROS_ERROR("tool_frame not defined");
      return false;
    }

//    if (!m_controller_nh.getParam("controlled_joint",m_joint_names))
//    {
//      ROS_ERROR("controlled_joint not defined");
//      return false;
//    }

    urdf::Model model;
    model.initParam("robot_description");
    Eigen::Vector3d grav;
    grav << 0, 0, -9.806;
    m_chain = rosdyn::createChain(model,base_frame,tool_frame,grav);
//    chain->setInputJointsName(m_joint_names);
    m_thorQP.setDynamicsChain(m_chain);
  }

  
  Eigen::VectorXd qmax(m_nax);
  Eigen::VectorXd qmin(m_nax);
  Eigen::VectorXd Dqmax(m_nax);
  Eigen::VectorXd DDqmax(m_nax);
  Eigen::VectorXd tau_max(m_nax);
  
  m_initial_state.resize(2*m_nax);
  m_target_Dq.resize(nc*m_nax);
  m_target_scaling=1;
  m_next_Q.resize(m_nax);
  m_next_acc.resize(m_nax);
  m_scaling=1;
  
  m_target_Dq.setZero();
  m_next_Q=m_initial_state.head(m_nax);
  
  
  qmax.setConstant(3);
  qmin.setConstant(-3);
  qmin(0)=0;
  qmax(0)=1.7;
  Dqmax.setConstant(3);
  DDqmax.setConstant(3);
  tau_max.setConstant(100);
  
  m_thorQP.setIntervals(nc,m_nax,control_horizon,st);
  m_thorQP.setWeigthFunction(lambda_acc,lambda_tau,lambda_jerk,lambda_scaling,lambda_clik);
  m_thorQP.setConstraints(qmax,qmin,Dqmax,DDqmax,tau_max);
  if (m_thorQP.needUpdate())
  {
    ROS_INFO_NAMED(m_root_nh.getNamespace(),"UPDATING THOR MATRICES");
    m_thorQP.updateMatrices();
  }
  
  return true;
}

void ThorController::starting(const ros::Time& time, const std::vector< double >& qini, const std::vector< double >& Dqini)
{
  thor::PrefilterController::starting(time,qini,Dqini);
  
  for (unsigned int i=0;i<qini.size();i++)
  {
    m_initial_state(i)=qini.at(i);
    m_initial_state(i+qini.size())=Dqini.at(i);
  }
  
  m_prefilter_pnt.positions   = qini;
  m_prefilter_pnt.velocities  = Dqini;
  m_prefilter_pnt.accelerations.resize(m_nax,0);
  m_prefilter_pnt.effort.resize(m_nax,0);
  m_prefilter_pnt.time_from_start=ros::Duration(0);
  
  m_thorQP.setInitialState( m_initial_state );
  m_prediction_time=m_thorQP.getPredictionTimeInstant();
  
}

void ThorController::stopping(const ros::Time& time)
{
  thor::PrefilterController::stopping(time);
}

bool ThorController::update(const ros::Time& time, const ros::Duration& period)
{
//   thor::PrefilterController::update(time,period);
  
//   m_thor_prefilter->interpolate(m_scaled_time,m_pnt,m_override*m_safe_override_1*m_safe_override_2);
  
  m_queue.callAvailable();
  
  m_mtx.lock();
  for (unsigned int iP=0;iP<m_prediction_time.size();iP++)
  {
    m_thor_prefilter->interpolate(m_scaled_time+ros::Duration(m_prediction_time(iP)),m_prefilter_pnt,m_global_override);
    for (unsigned int iAx=0;iAx<m_nax;iAx++)
    {
      m_target_Dq(iAx+iP*m_nax)=m_prefilter_pnt.velocities.at(iAx);
      if (iP==0)
        m_next_Q(iAx)=m_prefilter_pnt.positions.at(iAx);
    }
  }
  if (!m_is_in_path_tolerance)
    m_out_of_path_scaling=std::max(0.1,m_out_of_path_scaling*0.999);
  else
    m_out_of_path_scaling=0.99*m_out_of_path_scaling+0.01;
  m_target_scaling=m_global_override*m_out_of_path_scaling;
    
  m_thorQP.computedCostrainedSolution(m_target_Dq,m_next_Q,m_target_scaling,m_thorQP.getState(),m_next_acc,m_scaling);
  m_thorQP.updateState(m_next_acc);

  ROS_INFO_STREAM_THROTTLE(0.25,"error=" << (m_next_Q-m_thorQP.getState().head(m_nax)).transpose());
  ROS_INFO_STREAM_THROTTLE(0.25,"error=" << m_chain->getJointTorque(m_thorQP.getState().head(m_nax),m_thorQP.getState().tail(m_nax),m_next_acc));


  m_is_in_tolerance=true;
  m_is_in_path_tolerance=true;
  for (unsigned int iAx=0;iAx<m_nax;iAx++)
  {
    m_pnt.positions.at(iAx)=m_thorQP.getState()(iAx);
    m_pnt.velocities.at(iAx)=m_thorQP.getState()(iAx+m_nax);
    m_pnt.accelerations.at(iAx)=m_thorQP.getState()(iAx+2*m_nax);
    if ( std::abs(m_pnt.positions.at(iAx)-m_next_Q(iAx))>m_goal_tolerance)
      m_is_in_tolerance=false;
    if ( std::abs(m_pnt.positions.at(iAx)-m_next_Q(iAx))>m_path_tolerance)
      m_is_in_path_tolerance=false;
    
  }
  
  m_scaled_time+=period*m_scaling;
  m_time+=period;
  
  m_is_in_tolerance=m_is_in_tolerance && (m_scaled_time-m_thor_prefilter->trjTime()).toSec()>0;
  m_mtx.unlock();
  
  
  std_msgs::BoolPtr msg(new std_msgs::Bool());;
  msg->data=m_is_in_tolerance && m_is_in_path_tolerance;
  m_tolerance_pub.publish(msg);
  
  std_msgs::Float64Ptr scaling_msg(new std_msgs::Float64());
  scaling_msg->data=m_scaling;
  m_scaling_pub.publish(scaling_msg);
  
  return true;
}

bool ThorPosVelEffController::init(hardware_interface::PosVelEffJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  m_ctrl.reset(new ThorController());
  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw = hw;
  if (!m_ctrl->init(root_nh,controller_nh))
    return false;
  
  m_joint_names=m_ctrl->getJointNames();
  m_nAx=m_joint_names.size();
  
  return true;
}

bool ThorPosController::init(hardware_interface::PositionJointInterface* hw, ros::NodeHandle& root_nh, ros::NodeHandle& controller_nh)
{
  m_ctrl.reset(new ThorController());
  m_root_nh = root_nh;
  m_controller_nh = controller_nh;
  m_hw = hw;
  if (!m_ctrl->init(root_nh,controller_nh))
    return false;
  
  m_joint_names=m_ctrl->getJointNames();
  m_nAx=m_joint_names.size();
  return true;
}


}
